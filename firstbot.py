import telegram
import logging
from telegram import ReplyKeyboardMarkup, ParseMode
from telegram.ext import Updater, CommandHandler, RegexHandler

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - '
                           '%(message)s', level=logging.INFO)

logger = logging.getLogger(__name__)

def showloginfo(bot, update, func_name):
    chat_id = update.message.chat_id
    username = update.message.from_user.username
    firstname = update.message.from_user.first_name
    lastname = update.message.from_user.last_name
    verif_isbot = update.message.from_user.is_bot
    user_id = update.effective_user.id
    logger.info(f'{func_name} - {chat_id} - {user_id} - {username} - {firstname} - {lastname} - {verif_isbot}')

def start(bot, update):
    #Startando o bot, comando para inicializar.

    std_keyboard = [['STATUS'],
                    ['HELP']]
    reply_markup = ReplyKeyboardMarkup(std_keyboard,
                                       resize_keyboard=True)
    chat_id = update.message.chat_id
    bot.send_chat_action(chat_id=chat_id, action=telegram.ChatAction.TYPING)
    bot.send_message(chat_id=chat_id,
                     text='*Olá user, por favor escolha uma das opções abaixo:*',
                     parse_mode=ParseMode.MARKDOWN,
                     reply_markup=reply_markup)
    showloginfo(bot, update, 'start')

def status(bot, update):
    #Botão 1 gera mais dois botões, um de retorno pro comando /start (MENU).

    std_keyboard = [['PROGRESS'],
                    ['MENU']]
    reply_markup = ReplyKeyboardMarkup(std_keyboard,
                                       resize_keyboard=True)
    chat_id = update.message.chat_id
    bot.send_chat_action(chat_id=chat_id, action=telegram.ChatAction.TYPING)
    bot.send_message(chat_id=chat_id,
                     text='*Escolha PROGRESS para receber uma frase ou MENU para retornar:*',
                     parse_mode=ParseMode.MARKDOWN,
                     reply_markup=reply_markup)
    showloginfo(bot, update, 'status')

def help(bot, update):
    #Botão 2 gera mais dois botões, um de retorno pro comando /start (MENU).

    std_keyboard = [['ABOUT'],
                    ['MENU']]
    reply_markup = ReplyKeyboardMarkup(std_keyboard,
                                       resize_keyboard=True)
    chat_id = update.message.chat_id
    bot.send_chat_action(chat_id=chat_id, action=telegram.ChatAction.TYPING)
    bot.send_message(chat_id=chat_id,
                     text=f'*Escolha ABOUT para saber quem eu sou ou MENU para retornar:*',
                     parse_mode=ParseMode.MARKDOWN,
                     reply_markup=reply_markup)
    showloginfo(bot, update, 'help')

def about(bot, update):
    #Botão secundário 1

    chat_id = update.message.chat_id

    bot.send_chat_action(chat_id=chat_id, action=telegram.ChatAction.TYPING)

    bot.send_message(chat_id=chat_id,
                     text=f'*Olá, eu sou um bot feito pelo estudante do curso de graduação em Engenharia de Computação da UFC.'
                          f' O objetivo aqui era testar comandos fazendo um bot teste, adquirir conhecimentos com o bot para '
                          f'trabalhar em um projeto com outro bot posteriormente.*',
                     parse_mode=ParseMode.MARKDOWN)

    showloginfo(bot, update, 'about')

def progress(bot, update):
    #Botão secundário 2

    chat_id = update.message.chat_id
    bot.send_chat_action(chat_id=chat_id, action=telegram.ChatAction.TYPING)
    bot.send_message(chat_id=chat_id,
                     text=f'*Bot concluído!!! Bora para o próximo!*',
                     parse_mode=ParseMode.MARKDOWN)
    showloginfo(bot, update, 'progress')

def error(bot, update, error):
    #Errors de log causados por atualizações.
    logger.warning('Update "%s" caused error "%s"', update, error)

def main():

    TOKEN = 'token aqui'
    updater = Updater(token=TOKEN)

    updater.dispatcher.add_handler(CommandHandler('start', start))
    updater.dispatcher.add_handler(RegexHandler('STATUS', status))
    updater.dispatcher.add_handler(RegexHandler('HELP', help))
    updater.dispatcher.add_handler(RegexHandler('ABOUT', about))
    updater.dispatcher.add_handler(RegexHandler('PROGRESS', progress))
    updater.dispatcher.add_handler(RegexHandler('MENU', start))
    updater.dispatcher.add_error_handler(error)

    # Iniciando o bot
    updater.start_polling()

    # Executando o bot até o usuário pressionar Ctrl+C ou o processo receber SIGINT, SIGTERM ou SIGABRT
    updater.idle()


if __name__ == '__main__':
    print('Rodando...')
    main()